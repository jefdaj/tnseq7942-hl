{ stdenv }:

stdenv.mkDerivation {
  name = "tnseq7942v2-data";
  srcPath = builtins.toString ./.;
  builder = ./builder.sh;
}
