#!/usr/bin/env bash

source ${stdenv}/setup
cd ${srcPath}
mkdir -p $out
cp -Rv --dereference 170927_150SR_MS2 $out
