#!/usr/bin/env bash

# fill these in before running it
# note that the password needs to be shell-escaped
FTPSERV='gslserver.qb3.berkeley.edu'
FTPUSER='usernamehere'
FTPPASS='passwordhere'

cd "$( dirname "${BASH_SOURCE[0]}" )"

cmd="open -u $FTPUSER,$FTPPASS; mirror -c /170927_150SR_MS2 ./; exit"
cmd="lftp -e \"$cmd\" $FTPSERV"
echo "$cmd" && eval "$cmd"

cmd="open -u $FTPUSER,$FTPPASS; mirror -i L005 /171205_100SR_HS4KA ./; exit"
cmd="lftp -e \"$cmd\" $FTPSERV"
echo "$cmd" && eval "$cmd"
