#!/usr/bin/env bash

source ${stdenv}/setup
cd ${srcPath}
mkdir -p $out

for f in genes pool; do
  cp -Rv --dereference FEBA_SynE_160922/$f $out
done
