{ stdenv }:

stdenv.mkDerivation {
  name = "tnseq7942v2-feba";
  srcPath = builtins.toString ./.;
  builder = ./builder.sh;
}
