tnseq7942v2
===========

This is my second try at tnseq with the Golden lab's 7942 library. It should be
100% reproducible! Just `nix-build`, wait for a ton of things to compile, and
when it finally finishes open `result/index.html` in a browser.
