with builtins;
with import ./code/01-nixdeps;

let
  ###############
  # utility fns #
  ###############

  # FEBA data from Adam + Morgann
  # TODO also include my first set of experiments
  prev = toPath ./data/feba/FEBA_SynE_160922;

  # one function to do all the standard steps that work on a single treatment
  # takes a set where the attibute names are treatments and the values are lists of fastq files,
  # and one name to actually operate on
  # returns a count table for that treatment
  countCodes = treatments: treatmentName:
    let
      index      = treatmentName;
      fastqs     = getAttr treatmentName treatments;
      fastqfix   = callPackage ./code/02-fastqfix   { inherit index fastqs; inherit (pythonPackages) biopython; };
      multicodes = callPackage ./code/03-multicodes { inherit feba index fastqfix; };
    in
      multicodes;

  ###################################
  # miseq to test initial outgrowth #
  ###################################

  # miseq test fastq files grouped by treatment for barseq
  # and the ungrouped, raw files for counting total reads
  # these will be used in the first part of the analysis, but just for general stats (no barseqr)
  testUndetermined = [ (toPath ./data/fastq/170927_150SR_MS2/Undetermined_S0_L001_R1_001.fastq.gz) ];
  testOutgrowths = rec {
    hepesPlus  = [ (toPath ./data/fastq/170927_150SR_MS2/Niyogi/tnseq2_A9_S1_L001_R1_001.fastq.gz) ];
    hepesMinus = [ (toPath ./data/fastq/170927_150SR_MS2/Niyogi/tnseq2_A10_S2_L001_R1_001.fastq.gz) ];
    hepesBoth  = concatLists [ hepesPlus hepesMinus ];
  };
  testFastqFiles = concatLists [
    testOutgrowths.hepesPlus
    testOutgrowths.hepesMinus
    testUndetermined
  ];
  # make a count table per treatment, then merge them into one big table
  testCodeTables = map ( countCodes testOutgrowths ) ( attrNames testOutgrowths );
  testCombinebarseq = callPackage ./code/04-combinebarseq { inherit feba prev; codeTables = testCodeTables; };

  #######################
  # LL/NL/HL experiment #
  #######################

  # same as above, but for the LL vs HL experiments
  # (note that this time there's a time0 treatment, and no "both")
  llhlUndetermined = [ (toPath ./data/fastq/171205_100SR_HS4KA/Undetermined_S0_L001_R1_001.fastq.gz) ];
  llhlOutgrowths = rec {
    # TODO when does this come in?
    # TODO and what about WT, WTKmR do I need to do anything with them?
    time0 = [
      (toPath ./data/fastq/171205_100SR_HS4KA/Lane1345678/Niyogi/C1green_S50_L005_R1_001.fastq.gz)
      (toPath ./data/fastq/171205_100SR_HS4KA/Lane1345678/Niyogi/C2green_S51_L005_R1_001.fastq.gz)
      (toPath ./data/fastq/171205_100SR_HS4KA/Lane1345678/Niyogi/C3green_S52_L005_R1_001.fastq.gz)
      (toPath ./data/fastq/171205_100SR_HS4KA/Lane1345678/Niyogi/C4green_S53_L005_R1_001.fastq.gz)
    ];
    lowLight = [
      (toPath ./data/fastq/171205_100SR_HS4KA/Lane1345678/Niyogi/C5green_S54_L005_R1_001.fastq.gz)
      (toPath ./data/fastq/171205_100SR_HS4KA/Lane1345678/Niyogi/C6green_S55_L005_R1_001.fastq.gz)
      (toPath ./data/fastq/171205_100SR_HS4KA/Lane1345678/Niyogi/C7green_S56_L005_R1_001.fastq.gz)
      (toPath ./data/fastq/171205_100SR_HS4KA/Lane1345678/Niyogi/C8green_S57_L005_R1_001.fastq.gz)
    ];
    normalLight = [
      (toPath ./data/fastq/171205_100SR_HS4KA/Lane1345678/Niyogi/C9green_S58_L005_R1_001.fastq.gz)
      (toPath ./data/fastq/171205_100SR_HS4KA/Lane1345678/Niyogi/C10green_S59_L005_R1_001.fastq.gz)
      (toPath ./data/fastq/171205_100SR_HS4KA/Lane1345678/Niyogi/C11green_S60_L005_R1_001.fastq.gz)
      (toPath ./data/fastq/171205_100SR_HS4KA/Lane1345678/Niyogi/C12green_S61_L005_R1_001.fastq.gz)
    ];
    highLight = [
      (toPath ./data/fastq/171205_100SR_HS4KA/Lane1345678/Niyogi/D1green_S62_L005_R1_001.fastq.gz)
      (toPath ./data/fastq/171205_100SR_HS4KA/Lane1345678/Niyogi/D2green_S63_L005_R1_001.fastq.gz)
      (toPath ./data/fastq/171205_100SR_HS4KA/Lane1345678/Niyogi/D3green_S64_L005_R1_001.fastq.gz)
      (toPath ./data/fastq/171205_100SR_HS4KA/Lane1345678/Niyogi/D4green_S65_L005_R1_001.fastq.gz)
    ];
  };
  llhlFastqFiles = concatLists [
    llhlOutgrowths.lowLight
    llhlOutgrowths.normalLight
    llhlOutgrowths.highLight
    llhlUndetermined
  ];
  llhlCodeTables = map ( countCodes llhlOutgrowths ) ( attrNames llhlOutgrowths );
  llhlCombinebarseq = callPackage ./code/04-combinebarseq { inherit feba prev; codeTables = llhlCodeTables; };

  # miseq run was just to check library diversity; now continue to barseq with actual experiments
  exps = ./data/experiments.tsv;
  llhlBarseqr = callPackage ./code/05-barseqr {
    inherit feba exps;
    syne = prev;
    combinebarseq = llhlCombinebarseq;
  };

  # plot growth curves
  od750 = toPath ./data/hl-ll-od750.tsv;
  growthCurves = callPackage ./code/06-growthcurves {
    inherit od750;
  };

  # compare library diversity
  # keggDir = toPath ./data/kegg;
  genesTsv = toPath ./data/genes.tsv;
  keggDir = toPath ./code/keggpathways;
  analysis = callPackage ./code/07-analysis {
    inherit testFastqFiles testCodeTables testCombinebarseq;
    inherit keggDir growthCurves genesTsv;
    inherit llhlFastqFiles llhlCodeTables llhlCombinebarseq llhlBarseqr;
  };

in analysis
