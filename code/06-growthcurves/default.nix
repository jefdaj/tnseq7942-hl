{ stdenv, rWrapper, rPackages, od750 }:

let
  myR = rWrapper.override {
    packages = with rPackages; [
      cowplot
      dplyr
      ggplot2
      gridExtra
      tidyr
    ];
  };

in stdenv.mkDerivation {
  name = "tnseq-growthcurves-0.6";
  src = ./.;
  buildInputs = [ myR ];
  inherit od750;
  builder = ./builder.sh;
}
