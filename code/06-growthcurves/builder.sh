#!/usr/bin/env bash

cmd="Rscript ${src}/growthcurves.R ${od750} ${out}/plots.png"

source ${stdenv}/setup
mkdir -p $out
cp ${od750} $out
echo "$cmd" 2>&1 | tee ${out}/builder.log
eval "$cmd" 2>&1 | tee -a ${out}/builder.log
