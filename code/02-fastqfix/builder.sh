#!/usr/bin/env bash

source $stdenv/setup
mkdir -p $TMP $out
cmd="${src}/fastqfix.py $out/${index} ${fastqs}"
echo "$cmd" && eval "$cmd" 2>&1 | tee "$out"/builder.log 
