#!/usr/bin/env python2

# Usage: fastqfix.py <infastq> <outprefix>
# TODO instead of an error file, just put back with the rest? would simplfy it
# TODO allow multiple infastqs so you can group replicates at the same time?

import gzip
from Bio import SeqIO

PRESEQ  = 'CAGCGTACG'
POSTSEQ = 'AGAGACCTC'

def find_orientation(seq):
    'determine whether a read is forward, reverse, or neither'
    if seq.find(PRESEQ) != -1 and seq.find(POSTSEQ) != -1:
        return 'forward'
    seq = seq.reverse_complement()
    if seq.find(PRESEQ) != -1 and seq.find(POSTSEQ) != -1:
        return 'reverse'
    return 'neither'

def trim_to_preseq(seq):
    'trim so -nPreExpected of flipped and non-flipped sequences match (all 0)'
    index = seq.seq.find(PRESEQ)
    # TODO remove assertion?
    assert index != -1 # check that it includes the preseq before calling this
    newseq   = seq.seq[index:]
    newphred = seq.letter_annotations['phred_quality'][index:]
    del seq.letter_annotations['phred_quality']
    seq.seq = newseq
    seq.letter_annotations['phred_quality'] = newphred
    return seq

def reverse_complement(seq):
    "reverse a fastq sequence, fixing biopython's weird ID behavior"
    i = seq.id
    d = seq.description.lstrip(i)[1:] + ' flipped'
    seq = seq.reverse_complement()
    seq.id = i
    seq.description = d
    return seq

def main(outprefix, infastqs):
    with gzip.open(outprefix + '_fixed.fastq.gz', 'w') as fixed, \
         gzip.open(outprefix + '_error.fastq.gz', 'w') as error:
      for infastq in infastqs:
        with gzip.open(infastq, 'r') as orig:
            for s in SeqIO.parse(orig, 'fastq'):
                o = find_orientation(s.seq)
                if o == 'neither':
                    SeqIO.write(s, error, 'fastq')
                else:
                    if o == 'reverse':
                        s = reverse_complement(s)
                    s = trim_to_preseq(s)
                    SeqIO.write(s, fixed, 'fastq')

if __name__ == '__main__':
    from sys import argv
    main(argv[1], argv[2:])
