{ stdenv, biopython, index, fastqs }:

stdenv.mkDerivation {
  name = "tnseq7942v2-fastqfix";
  src = ./.;
  buildInputs = [ biopython ];
  inherit index fastqs;
  builder = ./builder.sh;
}
