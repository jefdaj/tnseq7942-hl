{ stdenv, rWrapper, rPackages, inotifyTools, pandoc, which, gzip, bc
, testFastqFiles, testCodeTables, testCombinebarseq
, keggDir, growthCurves, genesTsv
, llhlFastqFiles, llhlCodeTables, llhlCombinebarseq, llhlBarseqr }:

# TODO use the llhl ones!

let myR = rWrapper.override { packages = with rPackages; [
  dplyr
  ggplot2
  ggrepel
  knitr
  markdown
  rmarkdown
  tidyr
  pathview
  clusterProfiler
];};

in stdenv.mkDerivation rec {
  name = "tnseq7942v2-analysis";
  buildInputs = [ inotifyTools pandoc myR which gzip bc ];
  inherit testFastqFiles testCodeTables testCombinebarseq;
  inherit keggDir growthCurves genesTsv;
  inherit llhlFastqFiles llhlCodeTables llhlCombinebarseq llhlBarseqr;

  # auto-update index.html in the src dir itself when coding
  srcPath = builtins.toPath ./.;
  shellHook = ./shell.sh;

  # build final output in a separate nix-store path
  src = ./.;
  builder = ./builder.sh;
}
