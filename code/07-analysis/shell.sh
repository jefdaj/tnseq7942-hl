#!/usr/bin/env bash

echo "This will update index.html when index.Rmd changes..."

cd ${srcPath}
inotifywait -q -r -m ./. | while read event; do
  [[ "$event" =~ "CLOSE_WRITE,CLOSE index.Rmd" ]] || continue
  Rscript -e "library(rmarkdown); render('index.Rmd')"
done
