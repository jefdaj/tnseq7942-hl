#!/usr/bin/env bash

cmd="Rscript -e \"library(rmarkdown); render('index.Rmd')\""

source ${stdenv}/setup
cd $TMPDIR
cp ${src}/index.Rmd ./
echo "$cmd" 2>&1 | tee builder.log
eval "$cmd" 2>&1 | tee -a builder.log
mkdir -p $out
cp *.html *.tsv $out
