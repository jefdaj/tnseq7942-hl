with import <nixpkgs> {};

let
  myPerlPackages = with perlPackages; perlPackages // {
    BioPerl = callPackage ./bioperl.nix {};
  };

in with pkgs; pkgs // {
  perlPackages = myPerlPackages;
  feba = callPackage ./feba.nix { perlPackages = myPerlPackages; };
}
