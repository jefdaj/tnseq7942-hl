{ stdenv, feba, syne, exps, combinebarseq }:

stdenv.mkDerivation {
  name = "tnseq-barseqr-0.6";
  src = ./.;
  buildInputs = [ feba ];
  inherit syne exps combinebarseq;
  builder = ./builder.sh;
}
