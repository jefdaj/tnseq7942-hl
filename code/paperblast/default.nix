with import <nixpkgs> {};

let myPython = python2.withPackages (ps: with ps; [
  beautifulsoup4 # TODO remove
  lxml
]);

in stdenv.mkDerivation {
  name = "paper-blast";
  src = ./.;
  # TODO remove ipython
  buildInputs = [ python2Packages.ipython myPython ];
}
