#!/usr/bin/env python2

from sys import argv
from urllib2 import urlopen
from re import findall, sub
from bs4 import BeautifulSoup

def paper_blast_soup(locus_id):
    url = 'http://papers.genomics.lbl.gov/cgi-bin/litSearch.cgi?query=%s'
    html = urlopen(url % locus_id).read()
    soup = BeautifulSoup(html, 'lxml')
    return soup

# def paper_blast(locus_id):
#     soup = paper_blast_soup(locus_id)
#     ps  = soup.find('html').find('body').find_all('p')
#     uls = soup.find('html').find('body').find_all('ul')
#     lis = [soup.find('li') for ul in uls]
#     lis = [li for li in lis if len(li.find_all('a')) > 0]
#     ps = [p for p in ps if 'style' in p.attrs \
#                and p.attrs['style'] == 'margin-top: 1em; margin-bottom: 0em;']
#     # ps = ps + lis
# #     try:
# #         n_similar = int(findall('[0-9]{1,}', ps[1].contents[0])[0])
# #     except:
# #         n_similar = 0 # TODO test this
#     # out = []
#     # out.append(ps[1].contents[0])
#     return ps

def find_papers(p_elem):
    papers = []
    try:
        for li in list(p_elem.next_siblings)[0].find_all('li'):
            try:
                paper = li.find_all('a')[0]
                papers += ['&nbsp; ', '&nbsp; ', paper, '</br>']
            except IndexError:
                pass
    except AttributeError:
        pass
    return papers

def html_homologs(soup, locus_id):
    ps  = soup.find('html').find('body').find_all('p')
    keep = ['Papers about BLAST hits of %s:</br>' % locus_id]
    uls = soup.find('html').find('body').find_all('ul')
    lis = [soup.find('li') for ul in uls]
    lis = [li for li in lis if len(li.find_all('a')) > 0]
    ps  = [p for p in ps if 'style' in p.attrs \
                and p.attrs['style'] == 'margin-top: 1em; margin-bottom: 0em;']
    for p in ps:
        cs = list(p.children)[:3]
        keep.append('&nbsp; ')
        keep += cs
        keep.append('</br>')
        keep += find_papers(p)
    if len(keep) == 1:
        keep.append('&nbsp; none')
    keep.append('</br>')
    html = '\n'.join(str(k) for k in keep)
    html = sub('</?b>', '', html)
    return html

def main(locus_ids):
    for locus_id in locus_ids:
        soup = paper_blast_soup(locus_id)
        homologs = html_homologs(soup, locus_id)
        html = homologs
        print html

if __name__ == '__main__':
    main(argv[1:])
