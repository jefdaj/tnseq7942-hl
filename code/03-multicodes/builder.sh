#!/usr/bin/env bash

source ${stdenv}/setup
mkdir -p $out

# I include the error reads to get more accurate stats.
# They have to be put after the good ones with tac though,
# or FEBA will complain that too high a percentage are wrong.
fastqs="$(ls ${fastqfix}/*.fastq.gz | tac | xargs echo)"

# TODO remove index and use something generic?
cmd="MultiCodes.pl -out $out/${index} -index ${index}"
cmd="$cmd -preseq CAGCGTACG -postseq AGAGACCTC -nPreExpected 0"
cmd="gunzip -c $fastqs | $cmd &> $out/${index}.log"

echo "$cmd" && eval "$cmd" 2>&1 | tee $out/builder.log
