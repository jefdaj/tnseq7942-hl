{ stdenv, feba, index, fastqfix }:

stdenv.mkDerivation {
  name = "tnseq7942v2-multicodes";
  src = ./.;
  buildInputs = [ feba ];
  inherit index fastqfix;
  builder = ./builder.sh;
}
