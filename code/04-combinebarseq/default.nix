{ stdenv, feba, prev, codeTables }:

stdenv.mkDerivation {
  name = "tnseq-combinebarseq-0.6";
  src = ./.;
  buildInputs = [ feba ];
  inherit prev codeTables;
  builder = ./builder.sh;
}
